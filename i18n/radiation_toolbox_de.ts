<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>LayerBase</name>
    <message>
        <location filename="../layer/__init__.py" line="69"/>
        <source>Loading data...</source>
        <translation>Daten werden geladen...</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>{} invalid measurement(s) skipped (see message log for details)</source>
        <translation>{} ungültige Messungen wurden übersprungen (Details siehe Meldungsprotokoll)</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>Data loaded</source>
        <translation>Die Daten wurden geladen</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation>{} Punkte wurden geladen (in {:.2f} Sek.).</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="160"/>
        <source>Unable to create SQLite datasource: {}</source>
        <translation>SQLite-Datenquelle kann nicht erstellt werden: {}</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="297"/>
        <source>Style &apos;{}&apos; not found</source>
        <translation>Stil &apos;{}&apos; nicht gefunden</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="325"/>
        <source>Undefined style</source>
        <translation>Undefinierter Stil</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidget</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="198"/>
        <source>Load or select Safecast layer in order to display ader statistics.</source>
        <translation>Laden oder wählen Sie einen Safecast-Layer aus, um Statistiken anzuzeigen.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="234"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="252"/>
        <source>Load or select Safecast layer in order to display ader plot.</source>
        <translation>Laden Sie den Safecast-Layer oder wählen Sie ihn aus, um das Dosisleistungsdiagramm anzuzeigen.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="707"/>
        <source>Plot</source>
        <translation>Diagramm</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="333"/>
        <source>Load radiation data file</source>
        <translation>Strahlungsdatendatei laden</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Critical</source>
        <translation>Kritisch</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="384"/>
        <source>Unsupported file extension {}</source>
        <translation>Nicht unterstützte Dateierweiterung {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Failed to load input file &apos;{0}&apos;.

Details: {1}</source>
        <translation>Fehler beim Laden der Eingabedatei  &apos;{0}&apos;.

Details: {1}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="456"/>
        <source>Failed to apply style: {0}</source>
        <translation>Stil konnte nicht angewendet werden: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="481"/>
        <source>Invalid Safecast layer</source>
        <translation>Ungültige Safecast-Ebene</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>Save layer as new LOG file</source>
        <translation>Ebene als neue LOG-Datei speichern</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>LOG file (*.LOG)</source>
        <translation>LOG-Datei (*.LOG)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>Overwrite?</source>
        <translation>Überschreiben?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>File {} already exists. Do you want to overwrite it?.</source>
        <translation>Die Datei {} existiert bereits. Wollen Sie es überschreiben?.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Failed to save LOG file: {0}</source>
        <translation>LOG-Datei konnte nicht gespeichert werden: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Delete?</source>
        <translation>Löschen?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Do you want to delete {} selected features? This operation cannot be reverted.</source>
        <translation>Möchten Sie {} ausgewählte Punkte löschen? Dieser Vorgang ist irreversibel.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="591"/>
        <source>No features selected. Nothing to be deleled.</source>
        <translation>Nichts ausgewählt. Es gibt nichts zu löschen.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="628"/>
        <source>No layer loaded or selected</source>
        <translation>Es ist keine Ebene geladen oder ausgewählt</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>No active layer available.</source>
        <translation>Es ist keine aktive Ebene verfügbar.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="669"/>
        <source>Measured points</source>
        <translation>Messpunkte</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="674"/>
        <source>Layer statistics - {}</source>
        <translation>Ebene-Statistiken - {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Route information</source>
        <translation>Routeninformationen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average speed (km/h)</source>
        <translation>Durchschnittsgeschwindigkeit (km/h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total monitoring time</source>
        <translation>Gesamtmesszeit</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total distance (km)</source>
        <translation>Gesamtstrecke (km)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Radiation values</source>
        <translation>Strahlungswerte</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>maximum dose rate (microSv/h)</source>
        <translation>maximale Dosisleistung (microSv/h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average dose rate (microSv/h)</source>
        <translation>mittlere Dosisleistung (microSv/h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total dose (microSv)</source>
        <translation>Gesamtdosis (microSv)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="703"/>
        <source>Layer plot - {}</source>
        <translation>Ebenendiagramm - {}</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidgetBase</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="26"/>
        <source>Radiation Toolbox (DEV)</source>
        <translation>Radiation Toolbox (DEV)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="46"/>
        <source>Load and Edit</source>
        <translation>Laden und bearbeiten</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="282"/>
        <source>Style:</source>
        <translation>Stil:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="80"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="104"/>
        <source>Stats</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="134"/>
        <source>ADER statistics</source>
        <translation>ADER-Statistiken</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="148"/>
        <source>Plot</source>
        <translation>Diagramm</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="170"/>
        <source>ADER plot</source>
        <translation>ADER-Diagramm</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="178"/>
        <source>Maps</source>
        <translation>Karten</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="200"/>
        <source>Online maps</source>
        <translation>Online-Karten</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="212"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="228"/>
        <source>Map:</source>
        <translation>Karte:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="252"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="274"/>
        <source>Plot settings</source>
        <translation>Diagrammeinstellungen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="290"/>
        <source>Lines</source>
        <translation>Linien</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="295"/>
        <source>Points</source>
        <translation>Punkte</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="334"/>
        <source>Storage settings</source>
        <translation>Speichereinstellungen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="340"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="348"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="353"/>
        <source>GeoPackage</source>
        <translation>GeoPackage</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="358"/>
        <source>Memory</source>
        <translation>Speicher</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="379"/>
        <source>Import</source>
        <translation>Laden</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="382"/>
        <source>Import radiation data</source>
        <translation>Strahlungsdaten laden</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="385"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="394"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="397"/>
        <source>Save layer into new file</source>
        <translation>Speichern Sie die Ebene in einer neuen Datei</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="400"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="409"/>
        <source>Select</source>
        <translation>Wählen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="412"/>
        <source>Select features to cut</source>
        <translation>Wählen Sie die Punkte aus, die Sie löschen möchten</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="421"/>
        <source>Deselect</source>
        <translation>Abwählen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="424"/>
        <source>Deselect features</source>
        <translation>Punkte abwählen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="433"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="436"/>
        <source>Delete selected features</source>
        <translation>Ausgewählte Punkte löschen</translation>
    </message>
</context>
<context>
    <name>SafecastLayer</name>
    <message>
        <location filename="../safecast_layer.py" line="86"/>
        <source>ADER microSv/h</source>
        <translation type="obsolete">ADER microSv/h</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="87"/>
        <source>Local time</source>
        <translation type="obsolete">Místní čas</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="88"/>
        <source>Device</source>
        <translation type="obsolete">Zařízení</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="89"/>
        <source>Device ID</source>
        <translation type="obsolete">ID zařízení</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="90"/>
        <source>Datetime</source>
        <translation type="obsolete">Datum a čas</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="91"/>
        <source>CPM</source>
        <translation type="obsolete">CPM</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="92"/>
        <source>Pulses 5sec</source>
        <translation type="obsolete">Impulzů za 5s</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="93"/>
        <source>Pulses total</source>
        <translation type="obsolete">Impulzů celkem</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="94"/>
        <source>Validity</source>
        <translation type="obsolete">Platnost</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="95"/>
        <source>Latitude (deg)</source>
        <translation type="obsolete">Zem. šířka (°)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="96"/>
        <source>Hemisphere</source>
        <translation type="obsolete">Polokoule</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="97"/>
        <source>Longitude (deg)</source>
        <translation type="obsolete">Zem. délka (°)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="98"/>
        <source>East/West</source>
        <translation type="obsolete">Východ/Západ</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="99"/>
        <source>Altitude</source>
        <translation type="obsolete">Nadm. výška</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="100"/>
        <source>GPS Validity</source>
        <translation type="obsolete">Platnost GPS</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="101"/>
        <source>Sat</source>
        <translation type="obsolete">Sat</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="102"/>
        <source>HDOP</source>
        <translation type="obsolete">HDOP</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="103"/>
        <source>CheckSum</source>
        <translation type="obsolete">CheckSum</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="138"/>
        <source>Loading data...</source>
        <translation type="obsolete">Načítám data...</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation type="obsolete">{} bodů načteno (během {:.2f} sec).</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="119"/>
        <source>Failed to read input data. Line: {}</source>
        <translation>Eingabedaten konnten nicht geladen werden. Linie: {}</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="222"/>
        <source>unknown</source>
        <translation type="obsolete">neznámý</translation>
    </message>
</context>
<context>
    <name>SafecastLayerHelper</name>
    <message>
        <location filename="../layer/safecast.py" line="282"/>
        <source>Unable to retrive Safecast metadata for selected layer</source>
        <translation>Ich kann die Safecast-Metadaten für die ausgewählte Ebene nicht abrufen</translation>
    </message>
</context>
<context>
    <name>SafecastPlot</name>
    <message>
        <location filename="../tools/plot/safecast.py" line="85"/>
        <source>Distance (km)</source>
        <translation>Entfernung (km)</translation>
    </message>
    <message>
        <location filename="../tools/plot/safecast.py" line="90"/>
        <source>ADER (microSv/h)</source>
        <translation>ADER (microSv/h)</translation>
    </message>
</context>
<context>
    <name>self._layer</name>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>No valid date found. Unable to fix datetime.</source>
        <translation>Kein gültiges Datum gefunden. Datum/Uhrzeit kann nicht korrigiert werden.</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="537"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
</context>
</TS>
